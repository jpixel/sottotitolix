1
00:00:00,280 --> 00:00:04,240
Oggi fuori dai cancelli della Valascia l'attesa
era la medesima che si respira prima degli

2
00:00:04,440 --> 00:00:09,440
incontri che contano, mentre del gas lacrimogeno,
dei fumogeni e di un gruppo di ultras losannesi

3
00:00:09,680 --> 00:00:13,740
- ma non solo - che un mese e quattro giorni
fa rovinò la partita e quella che doveva

4
00:00:13,920 --> 00:00:17,360
essere una domenica di sport rimane solo un
brutto ricordo.

5
00:00:17,620 --> 00:00:22,820
Brutto, ma non abbastanza per tenere oltre
400 bambini - con mamma e papà al seguito

6
00:00:22,980 --> 00:00:24,440
- lontani dalla pista.

7
00:00:24,620 --> 00:00:27,720
Sono venuto a vedere gli allenamenti

8
00:00:27,900 --> 00:00:31,220
Sai cos'è successo qualche settima su questa
pista?

9
00:00:31,480 --> 00:00:32,380
Sì

10
00:00:32,480 --> 00:00:33,320
Cosa ne pensi?

11
00:00:33,480 --> 00:00:34,320
È brutto

12
00:00:34,560 --> 00:00:40,660
Hanno buttato i fumogeni e poi li scagliavano
contro la gente

13
00:00:41,160 --> 00:00:42,500
Hai avuto paura?

14
00:00:42,680 --> 00:00:43,300
Sì

15
00:00:43,520 --> 00:00:46,340
Chi è il tuo giocatore preferito?

16
00:00:46,720 --> 00:00:47,800
Matt D'Agostini

17
00:00:47,980 --> 00:00:49,400
Cosa vuoi dirgli?

18
00:00:49,620 --> 00:00:51,120
Come sta.

19
00:00:51,820 --> 00:00:55,520
Per chiederlo basta aspettare che si aprano
i cancelli e che i giocatori scendano sul

20
00:00:55,740 --> 00:00:56,820
ghiaccio per l'incontro.

21
00:00:57,060 --> 00:00:58,880
Questa volta con le famiglie.

22
00:00:59,160 --> 00:01:01,040
Come stai oggi?

23
00:01:01,460 --> 00:01:03,140
Sto alla grande.

24
00:01:03,520 --> 00:01:06,280
Ogni giorno come questo, che puoi passare
con le famiglie, divertirti e staccare un

25
00:01:06,580 --> 00:01:07,420
po', è un buon giorno

26
00:01:08,120 --> 00:01:10,980
È la squadra che ha voluto questa giornata?

27
00:01:11,420 --> 00:01:19,660
Sì, dopo quello che è successo ci piaceva
l'idea di organizzare qualcosa di diverso.

28
00:01:20,060 --> 00:01:21,040
È una bella idea

29
00:01:21,440 --> 00:01:25,100
Nei primi passaggi siamo un po' tesi, ma è
bello

30
00:01:25,560 --> 00:01:33,800
È una bellissima cosa, sentiremo dopo anche
il feedback delle famiglie e dei bambini...

31
00:01:34,220 --> 00:01:37,240
Feedback che in questo family day alla Valascia
è pressoché unanime.

32
00:01:37,700 --> 00:01:44,220
È sempre emozionante vedere i bambini che
sono contenti, soprattutto dopo quello che

33
00:01:44,600 --> 00:01:49,120
è successo con il Losanna, è giusto che
vedano anche l'altro lato dello sport.

34
00:01:49,400 --> 00:01:51,180
Com'è questa giornata?

35
00:01:51,660 --> 00:01:52,600
Bellissima!

36
00:01:52,880 --> 00:01:53,620
Bellissima!

37
00:01:53,880 --> 00:01:55,040
La cosa che ti è piaciuta di più?

38
00:01:55,260 --> 00:01:56,440
Lo spogliatoio!

39
00:01:56,860 --> 00:02:01,220
Anche a me... è stato tutto bello

40
00:02:01,880 --> 00:02:06,500
Finalmente possiamo vedere la Valascia, per
loro è stata un'emozione incredibile

41
00:02:07,000 --> 00:02:10,500
Vieni ancora a vedere l'Ambrì?

42
00:02:11,260 --> 00:02:13,700
Non so... sì!

43
00:02:14,360 --> 00:02:20,440
Non l'ho mai portato, ma vorrei... nonostante
quello che è successo

44
00:02:20,940 --> 00:02:26,520
Un’esperienza quella odierna che forse non
cancella quanto accaduto un mese fa ma che

45
00:02:26,980 --> 00:02:29,620
riappacifica la base bianco blu con squadra
e società.

46
00:02:29,920 --> 00:02:32,660
Un grande successo, un successo per lo sport.

47
00:02:33,140 --> 00:02:34,820
L'iniziativa è partita dalla squadra.

48
00:02:35,720 --> 00:02:42,480
Discuteremo con i giocatori come hanno vissuto
la trasformazione in pratica della loro idea.

49
00:02:43,180 --> 00:02:45,420
Se i giocatori ci stanno, continueremo a riproporla...

