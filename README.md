# README #

Questo progetto permette di convertire un file di sottotitoli scaricato da Youtube (in formato srt) a un file con le formattazioni TimedText del Player SRG SSR affinch� sia possibile avere i sottotitoli nativi, opzionali e personalizzabili dall'utente stesso.
Si presenta con una pagina html (a cui ci sono collegate librerie js e fogli stile bootstrap) con due textarea: la prima con il testo "input" di partenza (vale a dire il file srt scaricato da Youtube), la seconda con il testo "output" generato dallo script e formattato in maniera corretta per il TimedText del player SRG SSR.

### What is this repository for? ###

* Sottotitolix
* Version 1.0

### How do I get set up? ###

* html
* javascript, un paio di librerie
* (Bootstrapt per rendere tutto un po' pi� guardabile)
* Database configuration
* Il file html gira anche in locale
* Testato su Edge, funziona alla grande. Ma anche su altri va bene